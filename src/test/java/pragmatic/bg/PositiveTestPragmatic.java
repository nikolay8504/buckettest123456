package pragmatic.bg;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.*;

public class PositiveTestPragmatic {

    public WebDriver driver;

    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


    }

    @Test
    public void positiveTest() {
        //navigation to the Register Account page
        driver.get("http://shop.pragmatic.bg/");

        WebElement myAccaunt = driver.findElement(By.xpath("//div[@id='top-links']//a[@class='dropdown-toggle']//span[@class='hidden-xs hidden-sm hidden-md']"));
        myAccaunt.click();
        driver.findElement(By.linkText("Register")).click();
        //Make sure that you are in the right place if you want.
        assertEquals(driver.getTitle(), driver.getTitle(), "Register Account");


        // entering the account data which will be submitted
        driver.findElement(By.id("input-firstname")).sendKeys("Zhelyazko");

        driver.findElement(By.id("input-lastname")).sendKeys("Kolev");
        driver.findElement(By.id("input-email")).sendKeys("nikolay2945@gmail.com");
        driver.findElement(By.id("input-telephone")).sendKeys("08887987564");
        driver.findElement(By.id("input-password")).sendKeys("123456789");
        driver.findElement(By.id("input-confirm")).sendKeys("123456789");

        //get the Radio Button as WebElement using it`s value attribute

        WebElement subscribeYes = driver.findElement(By.xpath("//label[@class='radio-inline']//input[@value = '1']"));
        //Check if its already selected? otherwise select the Radio Button
        //by calling click() method

        if (!subscribeYes.isSelected()) {
            subscribeYes.click();


        }
        // Verify Radio Button is selected
        assertTrue(subscribeYes.isSelected());

        WebElement checkboxPrivacyPolicy = driver.findElement(By.xpath("//a[@class='agree']/following-sibling::input[@type='checkbox']"));
        //Verify the privacy policy is not checked by default
        assertFalse("The privacy policy was checked/accepted by default.", checkboxPrivacyPolicy.isSelected());

        //check the privacy policy
        checkboxPrivacyPolicy.click();
        //Verify Checkbox is Selected
        assertTrue(checkboxPrivacyPolicy.isSelected());

        // click the continue button

        driver.findElement(By.xpath("//a[@class='agree']/following-sibling::input[@type='submit']")).click();

        //Assert that your account has been created
        assertEquals(driver.getTitle(), "Account Registered  ", "Warning: E-Mail Address is already registered!");


    }

}


